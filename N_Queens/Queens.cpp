#include <iostream>
#include "Queens.h"

/**
 * Print the current board configuration for this solution.
 * Q = queen, '.' = empty position
 */
void Queens::print_board(std::vector<int> queens) {
    for (auto i = 0 ; i < problem_size; i++){
        std::cout  << std::endl;
        for (auto j = 0 ; j < problem_size; j++){
            // check if queen is in current columns
            queens[i] == j ? std::cout << 'Q' << '\t' : std::cout << '.' << '\t';
        }
    }
    std::cout  << std::endl;
}

/**
 * Print the board configurations for all solution.
 * Q = queen, '.' = empty position
 */
void Queens::print_solutions() {
    int i = 0;
    for(const auto &solution : solutions){
        std::cout  << "---------------" << std::endl;
        std::cout << "Solution: "<< i++ << std::endl;
        print_board(solution);
    }
}
