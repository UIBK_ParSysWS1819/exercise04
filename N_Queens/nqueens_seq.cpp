#include <iostream>
#include <chrono>
#include "Queens.h"
#include "SQueens.h"
#include "../chrono_timer.h"

/*
 * Calculating the number of solutions to the N-Queens problem. Calculating the number of solutions to the N-Queens
 * problem. The input parameter for the program is the number N, and its output is the number of possible solutions.
 */



int main(int argc, char** argv) {
    int N = 13; //default

    if(argc == 2) {
        N = std::stoi(argv[1]);
    }

    std::cout << "N: " << N << std::endl;

    SQueens queens = SQueens(N);

    {
        ChronoTimer timer("Sequential NQueens");
        queens.solve();
    }

    std::cout << "Amount solutions: " << queens.get_amount_solutions() << std::endl;

    return EXIT_SUCCESS;
}