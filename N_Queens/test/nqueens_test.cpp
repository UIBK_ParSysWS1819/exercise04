#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>
#include <chrono>
#include "../MPQueens.h"
#include "../SQueens.h"

namespace {
    // source: http://oeis.org/A000170/list
    unsigned long number_of_solutions[28] = {1,1,0,0,2,10,4,40,92,352,724,2680,14200,73712,
                                             365596,2279184,14772512,95815104,666090624,
                                             4968057848,39029188884,314666222712,2691008701644,
                                             24233937684440,227514171973736,2207893435808352,
                                             22317699616364044,234907967154122528};


    // To use a test fixture, derive a class from testing::Test.
    class EightQueens_test : public testing::Test {
    protected:
        EightQueens_test() = default;

        // virtual void SetUp() will be called before each test is run.
        // Can be used to initialize the variables.
        virtual void SetUp() {
            mpQueens = MPQueens(8);
            mpQueens.solve();
            sQueens = SQueens(8);
            sQueens.solve();
        }
        MPQueens mpQueens;
        SQueens sQueens;

    };

    TEST_F(EightQueens_test, amount_solution_seq){
        EXPECT_EQ(number_of_solutions[8], sQueens.get_amount_solutions());
    }

    TEST_F(EightQueens_test, amount_solution_omp){
        EXPECT_EQ(number_of_solutions[8], mpQueens.get_amount_solutions());
    }

    TEST_F(EightQueens_test, same_solution_amount_seq_omp){
        EXPECT_EQ(sQueens.get_amount_solutions(), mpQueens.get_amount_solutions());
    }

    TEST(MPQueens_test, valid_solutions_4Queens) {
        /* Solutions for 4 Queens:
         *___________________________
         *      0   1   2   3
         *
         * 0     .   Q   .   .
         * 1     .   .   .   Q
         * 2     Q   .   .   .
         * 3     .   .   Q   .
         *
         *___________________________
         *       0   1   2   3
         *
         * 0     .   .   Q   .
         * 1     Q   .   .   .
         * 2     .   .   .   Q
         * 3     .   Q   .   .
         *___________________________
         */

        MPQueens mpQueens = MPQueens(4);
        auto solutions = mpQueens.solve();
        std::vector<int > queens_solution_1 = {1,3,0,2};
        std::vector<int > queens_solution_2 = {2,0,3,1};

        EXPECT_EQ(mpQueens.get_amount_solutions(),2);

        bool found_solution_2 = std::find(solutions.begin(), solutions.end(), queens_solution_2) != solutions.end();
        bool found_solution_1 = std::find(solutions.begin(), solutions.end(), queens_solution_2) != solutions.end();

        EXPECT_TRUE(found_solution_1 && found_solution_2);
    }

    TEST(MPQueens_test, number_of_solutions) {
        for (int i= 0; i < 13; i++){
            MPQueens mpQueens = MPQueens(i);
            mpQueens.solve();
            EXPECT_EQ(number_of_solutions[i],mpQueens.get_amount_solutions());
        }
    }


    TEST(SeqQueens_test, valid_solutions_4Queens) {
        /* Solutions for 4 Queens:
         *___________________________
         *      0   1   2   3
         *
         * 0     .   Q   .   .
         * 1     .   .   .   Q
         * 2     Q   .   .   .
         * 3     .   .   Q   .
         *
         *___________________________
         *       0   1   2   3
         *
         * 0     .   .   Q   .
         * 1     Q   .   .   .
         * 2     .   .   .   Q
         * 3     .   Q   .   .
         *___________________________
         */

        SQueens sQueens = SQueens(4);
        auto solutions = sQueens.solve();
        std::vector<int > queens_solution_1 = {1,3,0,2};
        std::vector<int > queens_solution_2 = {2,0,3,1};

        EXPECT_EQ(sQueens .get_amount_solutions(),2);

        bool found_solution_2 = (std::find(solutions.begin(), solutions.end(), queens_solution_2) != solutions.end());
        bool found_solution_1 = (std::find(solutions.begin(), solutions.end(), queens_solution_2) != solutions.end());

        EXPECT_TRUE(found_solution_1 && found_solution_2);
    }

    TEST(SeqQueens_test, number_of_solutions) {
        for (int i= 0; i < 13; i++){
            SQueens sQueens  = SQueens(i);
            sQueens.solve();
            EXPECT_EQ(number_of_solutions[i],sQueens.get_amount_solutions());
        }
    }

    TEST(SpeedQueens_test, number_of_solutions) {
        auto start = std::chrono::system_clock::now();

        for (int i= 0; i < 13; i++){
            SQueens sQueens  = SQueens(i);
            sQueens.solve();
            EXPECT_EQ(number_of_solutions[i],sQueens.get_amount_solutions());
        }

        auto end = std::chrono::system_clock::now();
        auto elapsed_seq = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Sequential time: "<< elapsed_seq.count() << " ms" << std::endl;

        start = std::chrono::system_clock::now();

        // parallel program
        for (int i= 0; i < 13; i++){
            MPQueens mpQueens = MPQueens(i);
            mpQueens.solve();
            EXPECT_EQ(number_of_solutions[i],mpQueens.get_amount_solutions());
        }

        end = std::chrono::system_clock::now();
        auto elapsed_para = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        std::cout << "Parallel OpenMP time: "<< elapsed_para.count() << " ms" << std::endl;

        EXPECT_LE(elapsed_para.count(), elapsed_seq.count());
    }

}