# Benchmarking setup

## General
- create a bash script for the benchmark
- if available, try different compilers
- use different amount threads (1, 2, 4, 8, 16, etc.) and see the relation between the amount of threads and the speedup
- run it multiple times (e.g. 7) and build mean or median
- for each algorithm increased amounts of problem sizes (see below)

### N-Queens
- Starting at 1 and go until 16 by increasing with value += 2

### Mergesort
- Starting at 100 and go until 100.000.000 by increasing with value *= 10

### Monte-Carlo Pi-Estimator
- Starting at 1 and go until 1.000.000.000 by increasing with value *= 10
- Try out different methods of generating random numbers


# Times needed

N-Queens:
 - Sequential: ~ 70 min.
 - Parallel: ~ 45 min.

Mergesort:
 - Sequential: ~ 2h
 - Parallel: ~ 10 min.

Monte Carlo Pi-Estimator:
 - Sequential: ~ 1h
 - Parallel: ~ 25 min.