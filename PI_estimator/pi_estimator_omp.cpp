#include <iostream>
#include <math.h>
#include "pi_estimator.hpp"
#include "../chrono_timer.h"

/*
 * A Monte Carlo estimator for pi. The input should be the number of samples to
 * use in the estimation, the output is the approximation of pi which was computed.
 */

int main(int argc, char** argv) {
    int num_samples = 10000000; //default

    if(argc == 2) {
        num_samples = std::stoi(argv[1]);
    }

    std::cout << "Number of samples: " << num_samples << std::endl;

    double pi = 0.0;

    {
        ChronoTimer timer("OpenMP Pi Estimator");
        pi = monte_carlo_pi_omp(num_samples);
    }

    std::cout << "Calculated Pi: " << pi << std::endl;
    std::cout << "Actual Pi: " << M_PI << std::endl;

    return EXIT_SUCCESS;
}