#include <gtest/gtest.h>
#include "../pi_estimator.hpp"

TEST(SeqTest, getRand) {
    for(size_t i = 0; i<1000; i++){
        double rand = getRand();
        EXPECT_LE(rand, 1.0);
        EXPECT_GE(rand, 0.0);
    }
}

TEST(SeqTest, distance) {
    EXPECT_EQ(distance(1.0, 1.0), 2.0);
    EXPECT_EQ(distance(0.0, 0.0), 0);
    EXPECT_EQ(distance(1.0, 0), 1.0);
}

TEST(SeqTest, calculate_pi) {
    EXPECT_EQ(calculate_pi(100, 100), 4.0);
    EXPECT_EQ(calculate_pi(100, 0), 0.0);
    EXPECT_EQ(calculate_pi(100, 50), 2.0);
}

TEST(SeqTest, monte_carlo_hits) {
    size_t sizes [10] = { 1, 10, 100, 1000, 1234, 5346, 567, 321, 537, 777 };
    for (size_t i = 0; i<10; i++) {
        size_t hits = monte_carlo_hits(sizes[i]);
        EXPECT_LE(hits, sizes[i]);
        EXPECT_GE(hits, 0);
    }
}


TEST(SeqTest, monte_carlo_pi_seq) {
    size_t sizes [10] = { 1, 10, 100, 1000, 1234, 5346, 567, 321, 537, 777 };
    for (size_t i = 0; i<10; i++) {
        double pi = monte_carlo_pi_seq(sizes[i]);
        EXPECT_LE(pi, 4.0);
        EXPECT_GE(pi, 0.0);
    }
}

TEST(SeqTest, monte_carlo_pi_omp) {
    size_t sizes [10] = { 1, 10, 100, 1000, 1234, 5346, 567, 321, 537, 777 };
    for (size_t i = 0; i<10; i++) {
        double pi = monte_carlo_pi_omp(sizes[i]);
        EXPECT_LE(pi, 4.0);
        EXPECT_GE(pi, 0.0);
    }
}
