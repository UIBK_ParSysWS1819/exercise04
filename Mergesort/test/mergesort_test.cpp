#include <gtest/gtest.h>

#include "../Mergesort.h"

namespace {
    // To use a test fixture, derive a class from testing::Test.
    class Mergesort_test : public testing::Test {
    protected:
        int array_size = 10;

        Mergesort_test() : mergesort(Mergesort(array_size)) {}

        // virtual void SetUp() will be called before each test is run.
        // Can be used to initialize the variables.
        virtual void SetUp() {
            mergesort.fill_vector();
        }

        Mergesort mergesort;
    };

    TEST_F(Mergesort_test, fill_test) {
        // SetUp() before test fills the array so there should be values inside
        EXPECT_EQ(mergesort.get_values().size(), array_size);
        EXPECT_FALSE(mergesort.get_values().empty());

        mergesort.get_values().clear();

        EXPECT_EQ(mergesort.get_values().size(), 0);
        EXPECT_TRUE(mergesort.get_values().empty());
    }

    TEST_F(Mergesort_test, merge_test) {
        std::vector<int> vector1{ 1, 3, 5, 7 };
        std::vector<int> vector2{ 2, 4, 6, 8 };
        std::vector<int> result{ 1, 2, 3, 4, 5, 6, 7, 8 };

        std::vector<int> merged_vector = mergesort.merge(vector1, vector2);

        EXPECT_EQ(result, merged_vector);
        EXPECT_EQ(result.size(), merged_vector.size());
    }
    
    TEST_F(Mergesort_test, sort_test) {
        EXPECT_FALSE(mergesort.is_sorted());
        mergesort.print_values();

        mergesort.do_mergesort();

        EXPECT_TRUE(mergesort.is_sorted());
        mergesort.print_values();
    }
}