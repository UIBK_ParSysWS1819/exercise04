#include <iostream>
#include <vector>
#include <random>
#include "../chrono_timer.h"
#include "Mergesort.h"


// ~ 2h

/*
 * Merge sort. The input for your program should be the size of the array to sort. The generated array
 * should be filled randomly. The output is the result of a check that the array is actually sorted.
 */


int main(int argc, char** argv) {
    int size = 100; //default

    if(argc == 2) {
        size = std::stoi(argv[1]);
    }

    std::cout << "Array size: " << size << std::endl;

    //1. fill array with random values
    Mergesort mergesort(size);
    mergesort.fill_vector();
    //mergesort.print_values();

    //2. Do mergesort_seq
    {
        ChronoTimer timer("Sequential Mergesort");
        mergesort.do_mergesort();
    }

    //3. do check if sorting was successful
    if(mergesort.is_sorted()) {
        std::cout << "Sorted successfully" << std::endl;
        //mergesort.print_values();

        return EXIT_SUCCESS;
    }
    else {
        std::cout << "Sorting failed!" << std::endl;

        return EXIT_FAILURE;
    }
}