#include <iostream>
#include <random>
#include "../chrono_timer.h"

#include "Mergesort.h"


/* Constructors */
Mergesort::Mergesort() = default;

Mergesort::Mergesort(int size) : array_size(size) {
    // initialize array
    values.reserve(array_size);
}

/* Member functions */

void Mergesort::do_mergesort() {
#if defined(OMP)
    mergesort_omp(values);
#else
    mergesort_seq(values);
#endif
}

#if defined(OMP)
void Mergesort::mergesort_omp(std::vector<int> &values) {
    if(values.size() <= 1) {
        return;
    }

    auto half = values.size() / 2;
    std::vector<int> left_side(values.begin(), values.begin() + half);  // save first half in temporary vector
    std::vector<int> right_side(values.begin() + half, values.end());   // save second half in temporary vector

    #pragma omp task shared(left_side)
    mergesort_omp(left_side);   // sort left side

    #pragma omp task shared(right_side)
    mergesort_omp(right_side);  // sort right side

    #pragma omp taskwait

    values = merge(left_side, right_side);
}

#else
void Mergesort::mergesort_seq(std::vector<int> &values) {
    if(values.size() <= 1) {
        return;
    }

    auto half = values.size() / 2;
    std::vector<int> left_side(values.begin(), values.begin() + half);  // save first half in temporary vector
    std::vector<int> right_side(values.begin() + half, values.end());   // save second half in temporary vector

    mergesort_seq(left_side);   // sort left side
    mergesort_seq(right_side);  // sort right side

    values = merge(left_side, right_side);
}
#endif

std::vector<int> Mergesort::merge(std::vector<int> &left_side, std::vector<int> &right_side) {
    std::vector<int> merged_values;
    merged_values.reserve(left_side.size() + right_side.size());

    auto left_begin = left_side.begin();
    auto left_end = left_side.end();

    auto right_begin = right_side.begin();
    auto right_end = right_side.end();

    while(left_begin != left_end && right_begin != right_end) {
        if(*left_begin < *right_begin) {
            merged_values.push_back(*left_begin);
            left_begin++;
        }
        else {
            merged_values.push_back(*right_begin);
            right_begin++;
        }
    }

    // copy rest of left array
    while(left_begin != left_end) {
        merged_values.push_back(*left_begin);
        left_begin++;
    }

    // copy rest of right array
    while(right_begin != right_end) {
        merged_values.push_back(*right_begin);
        right_begin++;
    }

    return merged_values;
}

bool Mergesort::is_sorted() {
    for(unsigned i = 1; i < values.size(); i++) {
        if(values[i-1] > values[i]) {   // if current value is smaller than previous one, sorting failed
            return false;
        }
    }

    return true;
}

/* Utility functions */

void Mergesort::fill_vector() {
    // https://en.wikipedia.org/wiki/C%2B%2B11#Extensible_random_number_facility
    // http://itscompiling.eu/2016/04/11/generating-random-numbers-cpp/
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_int_distribution<int> uniform_dist(0, 100);

    for(int i = 0; i < array_size; i++) {
        values.push_back(uniform_dist(engine));
    }
}

void Mergesort::print_values() {
    for(int value : values) {
        std::cout << value << ", ";
    }
    std::cout << std::endl;
}

std::vector<int>& Mergesort::get_values() {
    return values;
}