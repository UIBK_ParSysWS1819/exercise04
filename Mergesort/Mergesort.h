#ifndef EXERCISE04_MERGESORT_H
#define EXERCISE04_MERGESORT_H


#include <vector>


class Mergesort {
    int array_size;
    std::vector<int> values;

public:
    Mergesort();

    explicit Mergesort(int array_size);

    std::vector<int> merge(std::vector<int> &left_side, std::vector<int> &right_side);
    void do_mergesort();

#if defined(OMP)
    void mergesort_omp(std::vector<int> &values);
#else
    void mergesort_seq(std::vector<int> &values);
#endif

    bool is_sorted();
    void fill_vector();
    void print_values();

    std::vector<int>& get_values();
};


#endif //EXERCISE04_MERGESORT_H
