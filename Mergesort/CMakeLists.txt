
set(SOURCE_FILES mergesort_seq.cpp Mergesort.cpp Mergesort.h)
add_executable(mergesort_seq ${SOURCE_FILES})
set_target_properties(mergesort_seq PROPERTIES COMPILE_FLAGS "-O0")

set(SOURCE_FILES mergesort_omp.cpp Mergesort.cpp Mergesort.h)
add_executable(mergesort_omp ${SOURCE_FILES})
set_target_properties(mergesort_omp PROPERTIES COMPILE_FLAGS "-DOMP -O0 -fopenmp" LINK_FLAGS "-fopenmp")

add_subdirectory(test)